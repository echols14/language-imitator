import model.Language;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LanguageImitator {
    public static final int DEFAULT_PRECEDING_CHARS_CONSIDERED = 2;
    public static final int DEFAULT_WORDS_TO_GENERATE = 1;

    //command keywords
    private static final String QUIT = "quit";
    private static final String HELP = "help";
    private static final String LOAD = "load";
    private static final String LANGUAGES = "languages";
    private static final String ANALYZE = "analyze";
    private static final String GENERATE = "generate";
    private static final String CLEAR = "clear";

    //static members
    private static LanguageImitator imitator = new LanguageImitator();

    //static functions

    public static void main(String[] args){
        System.out.println("Welcome to the language imitator.");
        System.out.printf("At any time, type '%s' to see a list of commands.\n", HELP);
        System.out.printf("Type '%s' to terminate.\n", QUIT);
        String command = "";
        String rootCommand = "";
        do{
            //get a line from the user
            Scanner input = new Scanner(System.in);
            System.out.println("\nPlease enter a command:");
            command = input.nextLine();
            String[] lineArgs = command.split("\\s+");

            //handle their input
            if(lineArgs.length == 0){ //no real input
                continue;
            }
            rootCommand = lineArgs[0].toLowerCase();
            if(rootCommand.equals(HELP)) help();
            else if(rootCommand.equals(LOAD)) load(lineArgs);
            else if(rootCommand.equals(LANGUAGES)) languages();
            else if(rootCommand.equals(ANALYZE)) analyze(lineArgs);
            else if(rootCommand.equals(GENERATE)) generate(lineArgs);
            else if(rootCommand.equals(CLEAR)) clear(lineArgs);
            else if(!rootCommand.equals(QUIT)){
                System.out.println("Command not recognized. Type 'help' for a list of commands.");
            }

        } while(!rootCommand.equals(QUIT));
        System.out.println("Program terminating...");
    }

    private static void help(){
        System.out.println("Available commands:\n");
        System.out.println(HELP);
        System.out.println("Displays this dialog.\n");
        System.out.println(QUIT);
        System.out.println("Terminates the program.\n");
        System.out.printf("%s [language] [filename] {number of preceding chars to consider}\n", LOAD);
        System.out.println("Reads text from the given file and saves the words to a dictionary associated with the given language name.");
        System.out.printf("Providing an argument of how many preceding characters to consider is optional, default value of %d.\n\n", DEFAULT_PRECEDING_CHARS_CONSIDERED);
        System.out.println(LANGUAGES);
        System.out.println("Lists the names of languages that have dictionary data loaded.\n");
        System.out.printf("%s [language]\n", ANALYZE);
        System.out.println("The loaded dictionary data associated with the given language name is processed in preparation for word generation.\n");
        System.out.printf("%s [language] {number of words to generate}\n", GENERATE);
        System.out.println("Generates potentially new words using a statistical distribution of character transitions as" +
                " found in the analyzed data associated with the given language name.");
        System.out.printf("Providing an argument of how many new words to generate is optional, default value of %d.\n\n", DEFAULT_WORDS_TO_GENERATE);
        System.out.printf("%s [language]\n", CLEAR);
        System.out.println("All data associated with the given language are removed from memory.\n");
    }

    private static void load(String[] lineArgs){
        String usage = "load [language] [filename] {number of preceding chars to consider}";
        //check and handle args
        if(lineArgs.length < 3){
            System.out.println("Insufficient arguments. Please use the following form:");
            System.out.println(usage);
            return;
        }
        String languageName = lineArgs[1];
        String fileName = lineArgs[2];
        int precedingCharsToConsider = DEFAULT_PRECEDING_CHARS_CONSIDERED;
        if(lineArgs.length >= 4){
            try {
                precedingCharsToConsider = Integer.parseInt(lineArgs[3]);
            } catch(NumberFormatException e){
                System.out.println("Improper arguments. Please use the following form:");
                System.out.println(usage);
                return;
            }
        }
        imitator.load(languageName, fileName, precedingCharsToConsider);
    }

    private static void languages(){
        if(imitator.languages.isEmpty()){
            System.out.println("There are no languages with data.");
            System.out.println("Use the 'load' command to load data.");
            return;
        }
        imitator.printLanguages();
    }

    private static void analyze(String[] lineArgs){
        //check and handle args
        if(lineArgs.length < 2){
            System.out.println("Insufficient arguments. Please use the following form:");
            System.out.println("analyze [language]");
            return;
        }
        String languageName = lineArgs[1];
        //make the actual call
        imitator.analyze(languageName);
    }

    private static void generate(String[] lineArgs){
        //check and handle args
        String usage = "generate [language] {number of words to generate}";
        if(lineArgs.length < 2){
            System.out.println("Insufficient arguments. Please use the following form:");
            System.out.println(usage);
            return;
        }
        String languageName = lineArgs[1];
        int wordCount = DEFAULT_WORDS_TO_GENERATE;
        if(lineArgs.length >= 3){
            try {
                wordCount = Integer.parseInt(lineArgs[2]);
            } catch(NumberFormatException e){
                System.out.println("Improper arguments. Please use the following form:");
                System.out.println(usage);
                return;
            }
        }
        //make the actual call
        imitator.generate(languageName, wordCount);
    }

    private static void clear(String[] lineArgs){
        //check and handle args
        String usage = "clear [language]";
        if(lineArgs.length < 2){
            System.out.println("Insufficient arguments. Please use the following form:");
            System.out.println(usage);
            return;
        }
        String languageName = lineArgs[1];
        imitator.clear(languageName);
    }

    //***NON-STATIC MEMBERS AND FUNCTIONS***//

    //members
    private Map<String, Language> languages;

    //constructor
    public LanguageImitator(){
        languages = new HashMap<>();
    }

    //functions

    public Language getLanguage(String languageName){
        return languages.getOrDefault(languageName, null);
    }

    public boolean newLanguage(String languageName, int precedingCharsToConsider){
        //language already exists
        if(languages.containsKey(languageName)) return false;
        //create the language
        Language newLanguage = new Language(languageName, precedingCharsToConsider);
        languages.put(languageName, newLanguage);
        return true;
    }

    public void removeLanguage(String languageName){
        languages.remove(languageName);
    }

    public void load(String languageName, String fileName, int precedingCharsToConsider){
	    //get the language object to load data into
	    boolean languageIsNew = false;
	    Language languageToLoad = getLanguage(languageName);
	    if(languageToLoad == null){
		    //language has no data yet, so make it
            System.out.printf("Language %s has no data yet; loading for the first time.\n", languageName);
		    newLanguage(languageName, precedingCharsToConsider);
		    languageToLoad = getLanguage(languageName);
		    languageIsNew = true;
	    }
	    else System.out.printf("Language %s already has data; adding to the existing data.\n", languageName);

	    //load the data into the language object
	    boolean fileFound = languageToLoad.readTrainingText(fileName);

	    //check success reading the file
	    if(!fileFound && languageIsNew){
		    //the new language object was created needlessly
		    removeLanguage(languageName);
	    }
	    if(fileFound){
		    System.out.println("File successfully loaded.");
	    }
    }

    public void analyze(String languageName){
        //get the language object to analyze
        Language languageToLoad = getLanguage(languageName);
        if(languageToLoad == null){
            System.out.println("The language specified has no data to analyze.");
            return;
        }

        //analyze the data in the language object
        languageToLoad.analyzeDictionary();
    }

    public void generate(String languageName, int wordCount){
        //get the language object to generate words from
        Language generatingLanguage = getLanguage(languageName);
        if(generatingLanguage == null){
            System.out.println("The language specified has no data to generate from.");
            return;
        }
        if(!generatingLanguage.isAnalyzed()){
            System.out.println("The language specified has not been analyzed.");
            return;
        }

        //generate the given number of words and print them
        List<String> generatedWords = generatingLanguage.generate(wordCount);
        if(generatedWords == null || generatedWords.isEmpty()){ //error checking
            System.out.println("There was a problem generating words from the given language.");
            System.out.println("The language may not have been analyzed yet.");
            return;
        }
        System.out.println("The following words were generated:");
        for(String newWord: generatedWords){
            System.out.println(newWord);
        }
    }

    public void clear(String languageName){
        //make sure that the language we need to remove actually exists
        Language languageToClear = getLanguage(languageName);
        if(languageToClear == null){
            System.out.printf("No language was found with the name %s; no data cleared.\n", languageName);
            return;
        }
        //remove the language
        removeLanguage(languageName);
        System.out.printf("All data cleared for %s.\n", languageName);
    }

    public void printLanguages(){
        //print the saved languages
        System.out.println("The following languages have data:");
        int i = 0;
        for(String languageName: languages.keySet()){
            String analyzedString = "";
            if(!languages.get(languageName).isAnalyzed()) analyzedString = " (not yet analyzed)";
            System.out.printf("(%d) %s%s\n", ++i, languageName, analyzedString);
        }
    }
}
