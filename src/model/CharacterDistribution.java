package model;

import java.util.Map;
import java.util.Random;

public class CharacterDistribution {
    //members
    private Map<Character, Integer> countMap;
    private int sum = 0;
    private static final Random rand = new Random();

    //constructor

    public CharacterDistribution(Map<Character, Integer> countMap) {
        this.countMap = countMap;
        for(int count: countMap.values()){
            sum += count;
        }
    }

    //functions

    public Character pickCharacter(){
        int n = rand.nextInt(sum);
        int localCount = 0;
        for(char key: countMap.keySet()){
            int value = countMap.get(key);
            localCount += value;
            if(n < localCount){
                return key;
            }
        }
        return null;
    }
}
