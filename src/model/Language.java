package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Language {
    //data members
    private String name;
    private int precedingCharsConsidered;
    private Set<String> dictionary;
    private CharacterDistribution initialDistribution;
    private Map<String, CharacterDistribution> transitionDistributions;
    private boolean analyzed = false;

    //constructor
    public Language(String name, int precedingCharsConsidered) {
        this.name = name;
        this.precedingCharsConsidered = precedingCharsConsidered;
        dictionary = new HashSet<>();
        initialDistribution = null;
        transitionDistributions = new HashMap<>();
    }

    //functions

    public boolean readTrainingText(String filename){
        try(Scanner text = new Scanner(new BufferedReader(new FileReader(filename)))) {
            while(text.hasNext()){
                String word = text.next().toLowerCase();
                dictionary.add(word);
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file '" + filename + "' could not be found.");
            return false;
        }
        return true;
    }

    public void analyzeDictionary(){
        Map<Character, Integer> initialCounts = new HashMap<>();
        Map<String, Map<Character, Integer>> transitionCounts = new HashMap<>();

        for(String word: dictionary){
            if(word.length() == 0){ //error checking
                continue;
            }

            //count the first character
            char initial = word.charAt(0);
            //if there's not yet a count for this character, the count is 0
            int oldStartCount = initialCounts.getOrDefault(initial, 0);
            //increment the count for this character
            initialCounts.put(initial, oldStartCount + 1);

            //count the transitions
            //i represents the index of the last character of the intro chunk
            for(int i = 0; i < word.length(); i++){
                //get the starting substring
                int startingIndex = i + 1 - precedingCharsConsidered;
                if(startingIndex < 0) startingIndex = 0;
                String current = word.substring(startingIndex, i+1);

                //get the final char
                char next = '\0';
                if(i+1 < word.length()) next = word.charAt(i+1);

                //save the transition
                if(!transitionCounts.containsKey(current)){
                    //the start-side of the transition is new
                    transitionCounts.put(current, new HashMap<>());
                }
                Map<Character, Integer> countMap = transitionCounts.get(current);
                //if there's not yet a transition to this character, the count is 0
                int oldCount = countMap.getOrDefault(next, 0);
                //increment the count for this transition
                countMap.put(next, oldCount + 1);
            }
        }

        //convert the count maps to CharacterDistributions
        initialDistribution = new CharacterDistribution(initialCounts);
        for(String key: transitionCounts.keySet()){
            Map<Character, Integer> value = transitionCounts.get(key);
            transitionDistributions.put(key, new CharacterDistribution(value));
        }

        analyzed = true;
        System.out.printf("Dictionary analyzed for %s.\n", name);
    }

    public List<String> generate(int wordCount){
        if(!analyzed) return null;
        List<String> results = new ArrayList<>(wordCount);
        int failedAttempts = 0;
        for(int n = 0; n < wordCount; n++) {
            //generate a new word

            //find a starting character
            Character first = initialDistribution.pickCharacter();
            if(first == null) return null;
            StringBuilder newWordBuilder = new StringBuilder(first.toString());

            //add transitions until we transition to the \0 character
            //i represents the index of the last character of the intro chunk
            for(int i = 0; i < newWordBuilder.length(); i++) {
                //get the starting substring
                int startingIndex = i + 1 - precedingCharsConsidered;
                if (startingIndex < 0) startingIndex = 0;
                String precedingSubstring = newWordBuilder.substring(startingIndex, i + 1);
                //get the next char
                CharacterDistribution nextDistribution = transitionDistributions.get(precedingSubstring);
                if(nextDistribution == null){
                    System.err.println("Language::generate(); No distribution found!");
                    break;
                }
                Character next = nextDistribution.pickCharacter();
                if(next == null){ //error checking
                    System.err.println("Language::generate(); null character picked!");
                    break;
                }
                else if(next == '\0'){ //end of the word
                    break;
                }
                //put the picked char on the word
                newWordBuilder.append(next);
            }
            String newWord = newWordBuilder.toString();
            //if the constructed word is a real one in the dictionary or one we already made, try again
            //if we try 100 times to get a good word and fail, it will let a trash one in
            if(dictionary.contains(newWord) || (results.contains(newWord)) && failedAttempts++ < 100){
                n--;
                continue;
            }
            failedAttempts = 0;
            //save the constructed word to our list
            results.add(newWord);
        }
        return results;
    }

    //getters
    public String getName() {
        return name;
    }

    public boolean isAnalyzed() {
        return analyzed;
    }
}
